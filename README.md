Pav' Layman Overlay
===================

My own collection of ebuilds I need on various Gentoo machines.

To add this overlay to your local layman list, you could create an XML file with the following contents under your `/etc/layman/overlays/`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE repositories SYSTEM "/dtd/repositories.dtd">
<repositories xmlns="" version="1.0">
<repo quality="experimental" status="official">
    <name>pav</name>
    <description>A private overlay with packages needed to build the cm-pgm-sync-httpd</description>
    <owner type="project">
        <email>pavpen@gmail.com</email>
	<name>Pavel Penev</name>
    </owner>
    <source type="git">https://pavpen@bitbucket.org/pavpen/pav-layman-overlay.git</source>
    <source type="git">git@bitbucket.org:pavpen/pav-layman-overlay.git</source>
</repo>
</repositories>
```

Then you should be able to do:

```
# layman -L
# layman -a pav
```

--
Pav
